<aside id="sidebar" class="column">
		<form class="quick_search">
			<input type="text" value="Quick Search" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>IMPORT DATA</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="importpenduduk.php">Penduduk</a></li>
			<li class="icn_edit_article"><a href="importkeluarga.php">Keluarga</a></li>
			<li class="icn_categories"><a href="insertkelurahan.php">Kelurahan</a></li>
			<li class="icn_tags"><a href="prosesNaiveBayes.php">Klasifikasi Naive Bayes</a></li>
            <li class="icn_tags"><a href="trainingNaiveBayes.php">Training Naive Bayes</a></li>
        </ul>
		<h3>Users</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="addnewuser.php">Add New User</a></li>
			<li class="icn_view_users"><a href="viewusers.php">View Users</a></li>
			<li class="icn_profile"><a href="index.php">Your Profile</a></li>
		</ul>
		<h3>Media</h3>
		<ul class="toggle">
					<li class="icn_photo"><a href="#">Grafik</a></li>

			<!--<li class="icn_folder"><a href="#">File Manager</a></li>
			<li class="icn_photo"><a href="#">Gallery</a></li>
			<li class="icn_audio"><a href="#">Audio</a></li>
			<li class="icn_video"><a href="#">Video</a></li>-->
		</ul>
		<h3>Admin</h3>
		<ul class="toggle">
			<!--<li class="icn_settings"><a href="#">Options</a></li>
			<li class="icn_security"><a href="#">Security</a></li>-->
			<li class="icn_jump_back"><a href="logout.php">Logout</a></li>
		</ul>
		
		<footer>
			<hr />
			<p><strong>Copyright &copy; 2014 Website Admin</strong></p>
			
		</footer>
	</aside><!-- end of sidebar -->