-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 27, 2014 at 02:23 PM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_penduduk`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `NIP` varchar(20) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Alamat` text NOT NULL,
  `Jabatan` varchar(20) NOT NULL,
  PRIMARY KEY (`NIP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`NIP`, `Nama`, `username`, `Password`, `Alamat`, `Jabatan`) VALUES
('123456789', 'administrator sistem', 'admin', 'admin', 'adm', 'admin kecamatan');

-- --------------------------------------------------------

--
-- Table structure for table `hasil`
--

CREATE TABLE IF NOT EXISTS `hasil` (
  `idhasil` int(11) NOT NULL AUTO_INCREMENT,
  `hasildm` varchar(20) NOT NULL,
  `sebelumdm` varchar(20) NOT NULL,
  `idpenduduk` varchar(20) NOT NULL,
  PRIMARY KEY (`idhasil`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=157 ;

--
-- Dumping data for table `hasil`
--

INSERT INTO `hasil` (`idhasil`, `hasildm`, `sebelumdm`, `idpenduduk`) VALUES
(118, '', 'Sejahtera', '10101'),
(119, '', 'Sejahtera', '10102'),
(120, '', 'Sejahtera', '10103'),
(121, '', 'Sejahtera', '10104'),
(122, '', 'Sejahtera', '10105'),
(123, '', 'Sejahtera', '10106'),
(124, '', 'Sejahtera', '10107'),
(125, '', 'Sejahtera', '10108'),
(126, '', 'Sejahtera', '10109'),
(127, '', 'Sejahtera', '10110'),
(128, '', 'Sejahtera', '10111'),
(129, '', 'Prasejahtera', '10112'),
(130, '', 'Sejahtera', '10113'),
(131, '', 'Sejahtera', '10114'),
(132, '', 'Sejahtera', '10115'),
(133, '', 'Sejahtera', '10116'),
(134, '', 'Sejahtera', '10117'),
(135, '', 'Sejahtera', '10118'),
(136, '', 'Sejahtera', '10128'),
(137, '', 'Sejahtera', '10119'),
(138, '', 'Sejahtera', '20201'),
(139, '', 'Sejahtera', '20202'),
(140, '', 'Sejahtera', '20203'),
(141, '', 'Sejahtera', '20204'),
(142, '', 'Sejahtera', '20205'),
(143, '', 'Sejahtera', '20206'),
(144, '', 'Sejahtera', '20207'),
(145, '', 'Sejahtera', '20208'),
(146, '', 'Sejahtera', '20209'),
(147, '', 'Sejahtera', '20300'),
(148, '', 'Sejahtera', '20301'),
(149, '', 'Sejahtera', '20302'),
(150, '', 'Sejahtera', '20303'),
(151, '', 'Sejahtera', '20304'),
(152, '', 'Sejahtera', '20305'),
(153, '', 'Sejahtera', '20306'),
(154, '', 'Sejahtera', '20307'),
(155, '', 'Sejahtera', '20308'),
(156, '', 'Sejahtera', '20309');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE IF NOT EXISTS `kelurahan` (
  `idkel` int(11) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(100) NOT NULL,
  PRIMARY KEY (`idkel`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`idkel`, `Nama`) VALUES
(1, 'Sariak'),
(2, 'Ophir'),
(3, 'Mahakarya'),
(4, 'Kapar Selatan'),
(5, 'Kapar Timur'),
(6, 'Jambak Selatan'),
(7, 'Padang Laweh'),
(8, 'Kapar Utara'),
(9, 'Lubuk Pudiang'),
(10, 'Kapal Siro'),
(11, 'Giri Maju'),
(12, 'Pujo Rahayu'),
(13, 'Sungai Talang'),
(14, 'Simpang III'),
(15, 'Rumbai Pesisir');

-- --------------------------------------------------------

--
-- Table structure for table `penduduk`
--

CREATE TABLE IF NOT EXISTS `penduduk` (
  `NIK` varchar(20) NOT NULL,
  `Nama` varchar(200) NOT NULL,
  `Pendapatan` varchar(20) NOT NULL,
  `Pekerjaan` varchar(20) NOT NULL,
  `Status` varchar(20) NOT NULL,
  `PendidikanTerakhir` varchar(20) NOT NULL,
  `JaminanKesehatan` varchar(20) NOT NULL,
  `KepemilikanRumah` varchar(20) NOT NULL,
  `JumlahAnggotaKeluarga` int(20) NOT NULL,
  `idkel` int(11) NOT NULL,
  PRIMARY KEY (`NIK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penduduk`
--

INSERT INTO `penduduk` (`NIK`, `Nama`, `Pendapatan`, `Pekerjaan`, `Status`, `PendidikanTerakhir`, `JaminanKesehatan`, `KepemilikanRumah`, `JumlahAnggotaKeluarga`, `idkel`) VALUES
('10101', 'Bowo', '1200000', 'BERDAGANG', 'MENIKAH', 'SMA', 'TIDAK ', 'KONTRAK/SEWA', 3, 1),
('10102', 'Yanto', '3000000', 'BUMN', 'MENIKAH', 'SMA', 'ADA', 'DINAS', 4, 1),
('10103', 'Agus Mulyono', '1500000', 'BERTANI', 'MENIKAH', 'SD', 'TIDAK', 'PRIBADI', 2, 1),
('10104', 'Rizky', '2500000', 'PNS', 'BELUM MENIKAH', 'D3', 'ADA', 'PRIBADI', 1, 2),
('10105', 'Suhartono', '900000', 'BERTANI', 'MENIKAH', 'SMP', 'TIDAK', 'KONTRAK/SEWA', 2, 3),
('10106', 'Yunanto Jaya', '2000000', 'PNS', 'MENIKAH', 'S1', 'ADA', 'DINAS', 2, 4),
('10107', 'Bachtiar Lubis', '2500000', 'BUMN', 'MENIKAH', 'SMA', 'ADA', 'DINAS', 3, 2),
('10108', 'Eko Budiatno', '1000000', 'BERTANI', 'BELUM MENIKAH', 'SMA', 'TIDAK', 'KONTRAK/SEWA', 1, 5),
('10109', 'Teguh Prasetya', '2000000', 'PNS', 'MENIKAH', 'SMA', 'ADA', 'DINAS', 3, 6),
('10110', 'A.Barus', '2200000', 'PNS', 'MENIKAH', 'D1', 'ADA', 'DINAS', 2, 7),
('10111', 'Ogop Surendra', '2900000', 'BUMN', 'BELUM MENIKAH', 'SMA', 'ADA', 'PRIBADI', 1, 8),
('10112', 'Budiono', '500000', 'PENGANGGURAN', 'BELUM MENIKAH', 'SD', 'TIDAK', 'KONTRAK/SEWA', 1, 9),
('10113', 'Iwan Rudiawan', '6000000', 'BUMN', 'MENIKAH', 'SMA', 'ADA', 'DINAS', 3, 10),
('10114', 'Sleman', '5500000', 'BUMN', 'MENIKAH', 'S1', 'ADA', 'PRIBADI', 5, 11),
('10115', 'Yusuf ', '2200000', 'BERDAGANG', 'BELUM MENIKAH', 'SMA', 'TIDAK', 'PRIBADI', 1, 11),
('10116', 'Darma Tarigan', '5000000', 'PNS', 'MENIKAH', 'D2', 'ADA', 'PRIBADI', 4, 12),
('10117', 'Ngamenken', '3500000', 'PNS', 'MENIKAH', 'D3', 'ADA', 'DINAS', 4, 13),
('10118', 'Ahmad', '3000000', 'PNS', 'MENIKAH', 'SMA', 'ADA', 'PRIBADI', 4, 14),
('10128', 'Bagong', '2000000', 'BERDAGANG', 'BELUM MENIKAH', 'D3', 'TIDAK', 'KONTRAK/SEWA', 1, 8),
('10119', 'Jhon Daulat', '1000000', 'PENGANGGURAN', 'BELUM MENIKAH', 'SD', 'TIDAK', 'KONTRAK/SEWA', 1, 14),
('20201', 'Frengki', '1000000', 'PENGANGGURAN', 'MENIKAH', 'SD', 'TIDAK', 'PRIBADI', 4, 9),
('20202', 'Suyatno', '1300000', 'BERTANI', 'BELUM MENIKAH', 'SMP', 'TIDAK', 'PRIBADI', 1, 9),
('20203', 'Mardan', '2500000', 'BUMS', 'MENIKAH', 'SMA', 'ADA', 'DINAS', 4, 2),
('20204', 'Arlianto', '2500000', 'PNS', 'MENIKAH', 'SMA', 'ADA', 'DINAS', 4, 13),
('20205', 'Siagian', '1200000', 'BERDAGANG', 'MENIKAH', 'SMP', 'TIDAK', 'PRIBADI', 3, 14),
('20206', 'Sinarta Sitepu', '3500000', 'BUMN', 'MENIKAH', 'S1', 'ADA', 'DINAS', 4, 8),
('20207', 'Alatan Hasibuan', '7000000', 'BUMS', 'MENIKAH', 'D3', 'ADA', 'PRIBADI', 4, 7),
('20208', 'Viktor S', '6500000', 'BUMN', 'MENIKAH', 'SMA', 'ADA', 'PRIBADI', 4, 11),
('20209', 'M.Yusuf ', '2500000', 'BERTANI', 'MENIKAH', 'SMP', 'TIDAK', 'DINAS', 3, 10),
('20300', 'Berkat', '5000000', 'PNS', 'MENIKAH', 'D4', 'ADA', 'PRIBADI', 5, 6),
('20301', 'Yatno ', '6000000', 'DOKTER', 'MENIKAH', 'S1', 'ADA', 'PRIBADI', 4, 6),
('20302', 'Darpin ', '2300000', 'BERDAGANG', 'MENIKAH', 'D1', 'ADA', 'PRIBADI', 3, 13),
('20303', 'Febrianta', '3000000', 'BUMS', 'MENIKAH', 'D3', 'ADA', 'KONTRAK/SEWA', 4, 14),
('20304', 'Aan', '3400000', 'BUMS', 'MENIKAH', 'SMA', 'ADA', 'KONTRAK/SEWA', 3, 14),
('20305', 'Andreas Barus', '9000000', 'BUMS', 'MENIKAH', 'S2', 'ADA', 'PRIBADI', 6, 2),
('20306', 'Sion ', '7000000', 'BUMN', 'MENIKAH', 'D4', 'ADA', 'DINAS', 5, 9),
('20307', 'Martin ', '3200000', 'BUMS', 'MENIKAH', 'S1', 'ADA', 'PRIBADI', 4, 7),
('20308', 'Khairul Mulyono', '3000000', 'BUMS', 'MENIKAH', 'SMA', 'ADA', 'DINAS', 3, 5),
('20309', 'Zulfikar', '1600000', 'BERDAGANG', 'MENIKAH', 'SMP', 'TIDAK', 'KONTRAK/SEWA', 4, 4);
